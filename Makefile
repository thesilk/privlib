VERSION=$(shell git describe --abbrev=4 --dirty --always --tags)
DEB_VERSION=$(shell echo ${VERSION} | cut -c 2-)

UID=$(shell id -u)
GID=$(shell id -g)

GITLAB_REGISTRY=registry.gitlab.com
GITLAB_PROJECT=thesilk/privlib

LDFLAGS=-ldflags "-w -s -X main.version=${VERSION}"

APP_DEB_PACKAGE := privlib-${VERSION}.deb

${APP_DEB_PACKAGE}: debian/*
	@rm -rf out/
	@mkdir -p out/package/opt/privlib
	@mkdir -p out/package/etc/privlib
	@mkdir -p out/package/var/opt/privlib
	@mkdir -p out/package/usr/local/bin
	@cp privlib out/package/opt/privlib/
	@cp debian/privlib.sh out/package/usr/local/bin/privlib
	@cp -r app/migrations out/package/opt/privlib/
	@mkdir -p out/package/DEBIAN
	@cp debian/control out/package/DEBIAN/
	@cp debian/postinst out/package/DEBIAN/
	@sed -i s/@VERSION@/${DEB_VERSION}/g out/package/DEBIAN/control
	@chmod -R 0775 out/package/DEBIAN
	@find ./debian -type d | xargs chmod 777
	@dpkg-deb -b out/package ${APP_DEB_PACKAGE}

DOCKER_PARAM=-it --rm\
			 -v ${PWD}:/app\
			 -v ${GOPATH}/pkg/mod/cache:/go/pkg/mod/cache\
			 --env UID=${UID}\
			 --env GID=${GID}\
			 -w /app\
			 ${GITLAB_REGISTRY}/${GITLAB_PROJECT}\

fmt:
	go fmt gitlab.com/...

vet:
	go vet gitlab.com/...

build:
	@go build ${LDFLAGS} -o privlib app/main.go

test: fmt vet
	go test ./... -count=1 -v

coverage: fmt vet
	go test -coverpkg=./... -count=1 -v -coverprofile=/tmp/coverage.out ./...
	go tool cover -func=/tmp/coverage.out

package: clean build ${APP_DEB_PACKAGE}
	@echo ${APP_DEB_PACKAGE}

run: build
	@echo './privlib runs in version ${VERSION}'
	@./privlib -config app/config.yaml -migrations app/migrations -database app/privlib.db

clean:
	@rm -f privlib*.db
	@rm -f privlib*.deb
	@rm -f privlib

install: package
	@sudo dpkg -i privlib*.deb

docker-image:
	@docker build -t ${GITLAB_REGISTRY}/${GITLAB_PROJECT} -f docker/build.Dockerfile .

docker-gitlab-registry-push: docker-image
	docker login ${GITLAB_REGISTRY}
	docker push ${GITLAB_REGISTRY}/${GITLAB_PROJECT}

docker-build:
	@docker run ${DOCKER_PARAM} /bin/bash -c "make build && chown ${UID}:${GID} privlib"

docker-test:
	@docker run ${DOCKER_PARAM} make test

docker-coverage:
	@docker run ${DOCKER_PARAM} make coverage

docker-package:
	@docker run ${DOCKER_PARAM} /bin/bash -c "make package && chown ${UID}:${GID} privlib privlib*.deb && rm -rf out/"

docker-privlib-build:

docker-privlib-run:
	@docker run -v ./app/config.yaml:/etc/ /bin/bash -c "make package && chown ${UID}:${GID} privlib privlib*.deb && rm -rf out/"


.PHONY: fmt\
		vet\
		build\
		test\
		coverage\
		package\
		run\
		clean\
		install\
		docker-image\
		docker-gitlab-registry-push\
		docker-build\
		docker-test\
		docker-coverage\
		docker-package
