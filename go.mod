module gitlab.com/thesilk/privlib

go 1.14

require (
	github.com/DATA-DOG/go-sqlmock v1.3.3
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/schema v1.1.0
	github.com/mattn/go-sqlite3 v1.11.0
	github.com/pkg/errors v0.8.1 // indirect
	github.com/pressly/goose v2.6.0+incompatible
	github.com/stretchr/testify v1.4.0
	gopkg.in/testfixtures.v2 v2.6.0
	gopkg.in/yaml.v2 v2.2.4
)
