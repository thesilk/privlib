# PRIVLIB

![Logo](privlib.png)


Privlib is a project to use public APIs to create your own movie and book collection.

## Installation

Download latest `*.deb`-release and install it. Alternatively you can compile the project yourself with

```bash
git clone https://gitlab.com/thesilk/privlib.git
cd privlib
make package
```

## Run locally

```bash
$ git clone https://gitlab.com/thesilk/privlib.git
$ cd privlib
$ make run &
$ cd frontend-ssr
$ yarn
$ yarn dev
$ xdg-open http://localhost:3000
```

## Technologies

- Golang
- Nuxt
- Sqlite3
- SSR
- Gitlab CI

## Screenshot

![Screenshot](screenshot_privlib.png)
![Screenshot](screenshot_movie_details.png)

Because of the copyright of the movie poster the screenshot contains only censored pictures. The app show uncensored pictures so it should only be used for private.

## ROADMAP

### v0.2

- add Frontend for API
  - search and add movies
  - filter own movies
  - movie overview + deleting and updating them

### v0.3

- add lent page
- mark items as lent
- notifications as reminder for lent items

### v1.0

- docker deployment on arm

### v1.1

- integrating third party book api
