-- +goose Up
CREATE TABLE IF NOT EXISTS movie (
  id INTEGER UNIQUE,
  title TEXT,
  original_title TEXT,
  overview TEXT,
  budget INTEGER,
  homepage TEXT,
  runtime INTEGER,
  vote_average REAL,
  vote_count INTEGER,
  revenue INTEGER,
  release_date string,
  popularity REAL,
  image TEXT);

CREATE TABLE IF NOT EXISTS genre (
  id INTEGER UNIQUE,
  name TEXT);

CREATE TABLE IF NOT EXISTS movie_has_genre (
  movie_id INTEGER,
  genre_id INTEGER);

CREATE TABLE IF NOT EXISTS credits_cast (
  movie_id INTEGER,
  person_id INTEGER,
  character TEXT,
  'order' INTEGER);

CREATE TABLE IF NOT EXISTS credits_crew (
  movie_id INTEGER,
  person_id INTEGER,
  department TEXT,
  job TEXT);

CREATE TABLE IF NOT EXISTS person (
  id INTEGER UNIQUE,
  name TEXT,
  birthday TEXT,
  death_day TEXT,
  gender INTEGER,
  biography TEXT,
  popularity REAL,
  place_of_birth TEXT,
  image TEXT);
