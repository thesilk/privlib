package controller

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"gitlab.com/thesilk/privlib/app/config"
)

type API struct{}

type Adapter struct {
	r      *mux.Router
	server *http.Server
}

func NewAdapter() Adapter {
	serverCfg := fmt.Sprintf("%s:%d", config.Cfg.GetServerHost(), config.Cfg.GetServerPort())

	r := mux.NewRouter()
	adapter := Adapter{r, &http.Server{Addr: serverCfg, Handler: handlers.CORS(handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"}), handlers.AllowedMethods([]string{"GET", "POST", "PUT", "HEAD", "OPTIONS"}), handlers.AllowedOrigins([]string{"*"}))(r)}}
	return adapter
}

func (a Adapter) ListenAndServe() {
	log.Printf("listening on %s", a.server.Addr)
	log.Fatal(a.server.ListenAndServe())
}

func (a Adapter) Shutdown() {
	a.server.Shutdown(context.Background())
}

func (a Adapter) HandleFunc(path string, f func(http.ResponseWriter, *http.Request)) *mux.Route {
	return a.r.NewRoute().Path(path).HandlerFunc(f)
}

func (a Adapter) NewAPIError(w http.ResponseWriter, status int, message string) {
	respondWithJSON(w, status, map[string]string{"error": message})
}

func (a Adapter) NewAPISuccess(w http.ResponseWriter, status int, payload interface{}) {
	respondWithJSON(w, status, payload)
}

func respondWithJSON(w http.ResponseWriter, status int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write(response)
}
