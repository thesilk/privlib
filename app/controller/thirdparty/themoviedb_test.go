package thirdparty

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSearchMoviesOk(t *testing.T) {
	server := startMockHttpServer(false)
	defer server.Close()

	api := NewTheMovieDbAPI(server.URL, "foo")

	response, err := api.SearchMovies("test", 1)

	assert.Nil(t, err)
	assert.Equal(t, expectedMovieResponse, response)
}

func TestSearchMoviesOfflineServer(t *testing.T) {
	api := NewTheMovieDbAPI("http://localhost:1234", "foo")

	_, err := api.SearchMovies("test", 1)

	assert.Contains(t, err.Error(), "couldn't search movies in themoviedb: Get \"http://localhost:1234/search/movie?api_key=foo&query=test&language=de&page=1\"")
	assert.Contains(t, err.Error(), "connection refused")
}

func TestSearchMoviesNoJson(t *testing.T) {
	server := startMockHttpServer(true)
	defer server.Close()

	api := NewTheMovieDbAPI(server.URL, "foo")

	_, err := api.SearchMovies("test", 1)

	assert.Equal(t, fmt.Errorf("couldn't deserialize response of themoviedb: unexpected end of JSON input"), err)
}

func TestSearchMovieOk(t *testing.T) {
	server := startMockHttpServer(false)
	defer server.Close()

	api := NewTheMovieDbAPI(server.URL, "foo")

	response, err := api.SearchMovie(1)

	assert.Nil(t, err)
	assert.Equal(t, expectedMovieSearchDetails.Cast(), response)
}

func TestSearchMovieOfflineServer(t *testing.T) {
	api := NewTheMovieDbAPI("http://localhost:1234", "foo")

	_, err := api.SearchMovie(1)

	assert.Contains(t, err.Error(), "couldn't search movie with id 1 in themoviedb: Get \"http://localhost:1234/movie/1?api_key=foo&language=de\"")
	assert.Contains(t, err.Error(), "connection refused")
}

func TestSearchMovieNoJson(t *testing.T) {
	server := startMockHttpServer(true)
	defer server.Close()

	api := NewTheMovieDbAPI(server.URL, "foo")

	_, err := api.SearchMovie(1)

	assert.Equal(t, fmt.Errorf("couldn't deserialize response of themoviedb: unexpected end of JSON input"), err)
}

func TestGetCreditsOk(t *testing.T) {
	server := startMockHttpServer(false)
	defer server.Close()

	api := NewTheMovieDbAPI(server.URL, "foo")

	response, err := api.GetCredits(1)

	assert.Nil(t, err)
	assert.Equal(t, expectedCredits.Cast(), response)
}

func TestGetCreditsOfflineServer(t *testing.T) {
	api := NewTheMovieDbAPI("http://localhost:1234", "foo")

	_, err := api.GetCredits(1)

	assert.Contains(t, err.Error(), "couldn't get credit for movie with id 1 in themoviedb: Get \"http://localhost:1234/movie/1/credits?api_key=foo\"")
	assert.Contains(t, err.Error(), "connection refused")
}

func TestGetCreditsNoJson(t *testing.T) {
	server := startMockHttpServer(true)
	defer server.Close()

	api := NewTheMovieDbAPI(server.URL, "foo")

	_, err := api.GetCredits(1)

	assert.Equal(t, fmt.Errorf("couldn't deserialize response of themoviedb: unexpected end of JSON input"), err)
}

func TestGetPeopleDetailsOk(t *testing.T) {
	server := startMockHttpServer(false)
	defer server.Close()

	api := NewTheMovieDbAPI(server.URL, "foo")

	response, err := api.GetPeopleDetails(1)

	assert.Nil(t, err)
	assert.Equal(t, expectedPerson.Cast(), response)
}

func TestGetPeopleDetailsOfflineServer(t *testing.T) {
	api := NewTheMovieDbAPI("http://localhost:1234", "foo")

	_, err := api.GetPeopleDetails(1)

	assert.Contains(t, err.Error(), "couldn't get person with id 1 in themoviedb: Get \"http://localhost:1234/person/1?api_key=foo&language=de\"")
	assert.Contains(t, err.Error(), "connection refused")
}

func TestGetPeopleDetailsNoJson(t *testing.T) {
	server := startMockHttpServer(true)
	defer server.Close()

	api := NewTheMovieDbAPI(server.URL, "foo")

	_, err := api.GetPeopleDetails(1)

	assert.Equal(t, fmt.Errorf("couldn't deserialize response of themoviedb: unexpected end of JSON input"), err)
}
