package thirdparty

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"

	"gitlab.com/thesilk/privlib/app/models"
)

type TheMovieDbAPIPort interface {
	SearchMovies(movieName string, page int) (models.MoviesResponseMovieDb, error)
	SearchMovie(id uint64) (models.MovieSearchDetails, error)
	GetCredits(id uint64) (models.Credits, error)
	GetPeopleDetails(id uint64) (models.Person, error)
}

type TheMovieDbAPI struct {
	url string
	key string
}

var _ TheMovieDbAPIPort = (*TheMovieDbAPI)(nil)

func NewTheMovieDbAPI(url, apiKey string) *TheMovieDbAPI {
	api := TheMovieDbAPI{
		url: url,
		key: apiKey,
	}

	return &api
}

func (api *TheMovieDbAPI) SearchMovies(movieName string, page int) (models.MoviesResponseMovieDb, error) {
	var response models.MoviesResponseMovieDb

	searchURL := fmt.Sprintf("%s/search/movie?api_key=%s&query=%s&language=de&page=%d", api.url, api.key, url.QueryEscape(movieName), page)

	resp, err := http.Get(searchURL)
	if err != nil {
		return response, fmt.Errorf("couldn't search movies in themoviedb: %v", err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(io.LimitReader(resp.Body, 1048576))
	if err != nil {
		return response, fmt.Errorf("couldn't read response body from themoviedb: %v", err)
	}

	if err := json.Unmarshal(body, &response); err != nil {
		return response, fmt.Errorf("couldn't deserialize response of themoviedb: %v", err)
	}

	return response, nil
}

func (api *TheMovieDbAPI) SearchMovie(id uint64) (models.MovieSearchDetails, error) {
	var movie models.MovieSearchDetailsImdb

	searchURL := fmt.Sprintf("%s/movie/%d?api_key=%s&language=de", api.url, id, api.key)

	resp, err := http.Get(searchURL)
	if err != nil {
		return models.MovieSearchDetails{}, fmt.Errorf("couldn't search movie with id %d in themoviedb: %v", id, err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(io.LimitReader(resp.Body, 1048576))
	if err != nil {
		return models.MovieSearchDetails{}, fmt.Errorf("couldn't read response body from themoviedb: %v", err)
	}

	if err := json.Unmarshal(body, &movie); err != nil {
		return models.MovieSearchDetails{}, fmt.Errorf("couldn't deserialize response of themoviedb: %v", err)
	}

	return movie.Cast(), nil
}

func (api *TheMovieDbAPI) GetCredits(id uint64) (models.Credits, error) {
	var credits models.CreditsImdb

	searchURL := fmt.Sprintf("%s/movie/%d/credits?api_key=%s", api.url, id, api.key)

	resp, err := http.Get(searchURL)
	if err != nil {
		return models.Credits{}, fmt.Errorf("couldn't get credit for movie with id %d in themoviedb: %v", id, err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(io.LimitReader(resp.Body, 1048576))
	if err != nil {
		return models.Credits{}, fmt.Errorf("couldn't read response body from themoviedb: %v", err)
	}

	if err := json.Unmarshal(body, &credits); err != nil {
		return models.Credits{}, fmt.Errorf("couldn't deserialize response of themoviedb: %v", err)
	}

	return credits.Cast(), nil
}

func (api *TheMovieDbAPI) GetPeopleDetails(id uint64) (models.Person, error) {
	var person models.PersonImdb

	searchURL := fmt.Sprintf("%s/person/%d?api_key=%s&language=de", api.url, id, api.key)

	resp, err := http.Get(searchURL)
	if err != nil {
		return models.Person{}, fmt.Errorf("couldn't get person with id %d in themoviedb: %v", id, err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(io.LimitReader(resp.Body, 1048576))
	if err != nil {
		return models.Person{}, fmt.Errorf("couldn't read response body from themoviedb: %v", err)
	}

	if err := json.Unmarshal(body, &person); err != nil {
		return models.Person{}, fmt.Errorf("couldn't deserialize response of themoviedb: %v", err)
	}

	return person.Cast(), nil
}
