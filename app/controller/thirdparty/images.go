package thirdparty

import (
	"encoding/base64"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
)

type Image struct{}

func (i Image) GetImage(url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", fmt.Errorf("couldn't fetch image from url: %v", err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(io.LimitReader(resp.Body, 5242880))
	if err != nil {
		return "", fmt.Errorf("couldn't read response body from image fetching: %v", err)
	}

	imgBase64Str := base64.StdEncoding.EncodeToString(body)
	contentType := resp.Header.Get("Content-Type")

	return fmt.Sprintf("data:%s;base64,%s", contentType, imgBase64Str), nil
}
