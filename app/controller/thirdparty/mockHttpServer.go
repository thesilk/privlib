package thirdparty

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"

	"gitlab.com/thesilk/privlib/app/models"
)

var expectedMovieOverview []*models.MovieOverviewMovieDb = []*models.MovieOverviewMovieDb{{
	ID:    1,
	Title: "title",
}}

var expectedMovieResponse models.MoviesResponseMovieDb = models.MoviesResponseMovieDb{
	Page:         1,
	TotalPages:   1,
	TotalResults: 1,
	Results:      expectedMovieOverview,
}

var expectedGenre []models.Genre = []models.Genre{{ID: 1, Name: "Action"}}
var expectedMovieSearchDetails models.MovieSearchDetailsImdb = models.MovieSearchDetailsImdb{
	ID:            1,
	Budget:        1,
	Genres:        expectedGenre,
	Homepage:      "homepage",
	Title:         "title",
	OriginalTitle: "originalTitle",
	Runtime:       123,
	VoteAverage:   2.3,
	VoteCount:     99,
	Revenue:       879,
	RelaseDate:    "01.01.1970",
	PosterPath:    "/poster.jpg",
	Popularity:    8.65,
	Overview:      "overview",
}

var expectedCredits models.CreditsImdb = models.CreditsImdb{
	ID:          1,
	CastDetails: []models.CastDetailsImdb{{}},
	CrewDetails: []models.CrewDetailsImdb{{}},
}

var expectedPerson models.PersonImdb = models.PersonImdb{
	ID:                 1,
	Birthday:           "01.01.1970",
	KnownForDepartment: "department",
	DeathDay:           "9.9.9999",
	Name:               "name",
	AlsoKnownAs:        []string{"name"},
	Gender:             2,
	Biography:          "biography",
	Popularity:         9.99,
	PlaceOfBirth:       "nowhere",
	ProfilePath:        "/path.jpg",
	Image:              "image",
	Adult:              false,
	Homepage:           "homepage",
}

func startMockHttpServer(bodyError bool) *httptest.Server {
	mux := http.NewServeMux()

	mux.HandleFunc("/search/movie", func(res http.ResponseWriter, req *http.Request) {
		if !bodyError {
			response, _ := json.Marshal(expectedMovieResponse)

			res.Header().Set("Content-Type", "application/json")
			res.WriteHeader(200)
			res.Write(response)

		} else {
			res.Write([]byte(""))
		}
	})
	mux.HandleFunc("/movie/1", func(res http.ResponseWriter, req *http.Request) {
		if !bodyError {
			response, _ := json.Marshal(expectedMovieSearchDetails)

			res.Header().Set("Content-Type", "application/json")
			res.WriteHeader(200)
			res.Write(response)

		} else {
			res.Write([]byte(""))
		}
	})
	mux.HandleFunc("/movie/1/credits", func(res http.ResponseWriter, req *http.Request) {
		if !bodyError {
			response, _ := json.Marshal(expectedCredits)

			res.Header().Set("Content-Type", "application/json")
			res.WriteHeader(200)
			res.Write(response)

		} else {
			res.Write([]byte(""))
		}
	})
	mux.HandleFunc("/person/1", func(res http.ResponseWriter, req *http.Request) {
		if !bodyError {
			response, _ := json.Marshal(expectedPerson)

			res.Header().Set("Content-Type", "application/json")
			res.WriteHeader(200)
			res.Write(response)

		} else {
			res.Write([]byte(""))
		}
	})
	mux.HandleFunc("/image/1", func(res http.ResponseWriter, req *http.Request) {
		if !bodyError {

			res.Header().Set("Content-Type", "text/plain")
			res.WriteHeader(200)
			res.Write([]byte("image content"))

		} else {
			res.Write([]byte(""))
		}
	})

	return httptest.NewServer(mux)
}
