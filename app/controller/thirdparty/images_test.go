package thirdparty

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetImageOk(t *testing.T) {
	server := startMockHttpServer(false)
	defer server.Close()

	image := Image{}
	response, err := image.GetImage(server.URL + "/image/1")

	assert.Nil(t, err)
	assert.Equal(t, "data:text/plain;base64,aW1hZ2UgY29udGVudA==", response)
}

func TestGetImageOfflineServer(t *testing.T) {
	image := Image{}
	_, err := image.GetImage("http://localhost:1234/image/1")

	assert.Contains(t, err.Error(), "couldn't fetch image from url: Get \"http://localhost:1234/image/1\"")
	assert.Contains(t, err.Error(), "connection refused")
}
