package controller

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/gorilla/schema"

	"gitlab.com/thesilk/privlib/app/models"
	"gitlab.com/thesilk/privlib/app/usecase"
)

func (a Adapter) readUrlParameters(r *http.Request) (models.MovieSearchParameter, error) {
	var param models.MovieSearchParameter

	err := schema.NewDecoder().Decode(&param, r.URL.Query())
	return param, err

}

func (a Adapter) readMovie(r *http.Request) (models.MovieSearchDetails, error) {
	var movie models.MovieSearchDetails

	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		return movie, fmt.Errorf("couldn't read body from request: %v", err)
	}

	if err := json.Unmarshal(body, &movie); err != nil {
		return movie, fmt.Errorf("couldn't unmarshal body: %v", err)
	}

	return movie, nil
}

func (a Adapter) MakeSearchMovie(searchMovie usecase.SearchMovie) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 64)
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't convert id parameter: %v", err))
			return
		}

		movie, err := searchMovie.Run(id)
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't get movie with id %d: %v", id, err))
			return
		}

		a.NewAPISuccess(w, http.StatusOK, movie)
	}
}

func (a Adapter) MakeSearchMovies(searchMovies usecase.SearchMovies) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		p, err := a.readUrlParameters(r)
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't read movie parameter: %v", err))
			return
		}

		movies, err := searchMovies.Run(p.MovieName, p.Page)
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't get movies with name %s: %v", p.MovieName, err))
			return
		}

		a.NewAPISuccess(w, http.StatusOK, movies)
	}
}

func (a Adapter) MakeGetMovie(getMovie usecase.GetMovie) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 64)
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't convert id parameter: %v", err))
			return
		}

		movie, err := getMovie.Run(id)
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't get movie with id %d: %v", id, err))
			return
		}

		a.NewAPISuccess(w, http.StatusOK, movie)
	}
}

func (a Adapter) MakeDeleteMovie(deleteMovie usecase.DeleteMovie) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("delete movie with id: %s", mux.Vars(r))
		id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 64)
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't convert id parameter: %v", err))
			return
		}

		err = deleteMovie.Run(id)
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't get movie with id %d: %v", id, err))
			return
		}

		a.NewAPISuccess(w, http.StatusAccepted, nil)
	}
}

func (a Adapter) MakeCreateMovie(createMovie usecase.CreateMovie, image usecase.GetImage) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 64)
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't convert id parameter: %v", err))
			return
		}

		err = createMovie.Run(id, image)
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't create movie: %v", err))
			return
		}

		a.NewAPISuccess(w, http.StatusCreated, nil)
	}
}

func (a Adapter) MakeGetMovies(getMovies usecase.GetMovies) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		p, err := a.readUrlParameters(r)
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't read movie parameter: %v", err))
			return
		}
		movies, err := getMovies.Run(p)
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't get movies: %v", err))
			return
		}

		a.NewAPISuccess(w, http.StatusOK, movies)
	}
}
