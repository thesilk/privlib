package controller

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/thesilk/privlib/app/config"
	"gitlab.com/thesilk/privlib/app/models"
	"gitlab.com/thesilk/privlib/app/usecase"
)

var expectedMovieOverview models.MovieOverviewMovieDb = models.MovieOverviewMovieDb{
	ID:         1,
	Title:      "test movie",
	PosterPath: "/abcdef.jpg",
}

var expectedMoviesResponse models.MoviesResponseMovieDb = models.MoviesResponseMovieDb{
	Page:         1,
	TotalResults: 1,
	TotalPages:   1,
	Results:      []*models.MovieOverviewMovieDb{&expectedMovieOverview},
}

var genre models.Genre = models.Genre{ID: 1, Name: "action"}
var cast models.CastDetails = models.CastDetails{
	Character: "peter",
	Gender:    0,
	ID:        1,
	Name:      "pan",
	Order:     1,
}

var credits models.Credits = models.Credits{
	ID:   1,
	Cast: []models.CastDetails{cast},
}

var person models.Person = models.Person{
	ID:    1,
	Name:  "Peter",
	Image: "base64",
}

var expectedMovieDetails models.MovieDetails = models.MovieDetails{
	ID:            1,
	Budget:        1234,
	Homepage:      "http://test.com",
	Title:         "title",
	OriginalTitle: "title",
	Runtime:       111,
	VoteAverage:   3.89,
	VoteCount:     125482,
	Revenue:       123456789,
	RelaseDate:    "01.01.1970",
	Popularity:    4.265,
	Overview:      "here is the plot",
	Image:         "base64",
	Genres:        []*models.Genre{&genre},
	Cast:          []models.CastDetails{cast},
}

var expectedMovieSearchDetails models.MovieSearchDetails = models.MovieSearchDetails{
	ID:            1,
	Budget:        1234,
	Homepage:      "http://test.com",
	Title:         "title",
	OriginalTitle: "title",
	Runtime:       111,
	VoteAverage:   3.89,
	VoteCount:     125482,
	Revenue:       123456789,
	RelaseDate:    "01.01.1970",
	Popularity:    4.265,
	Overview:      "here is the plot",
	Genres:        []models.Genre{genre},
}

type errResponse struct {
	Error string `json:"error"`
}

func TestMovieController(t *testing.T) {
	config.InitConfig("../config.yaml")

	t.Run("search movie", testSearchMovie)
	t.Run("search movie with error", testSearchMovieWithError)
	t.Run("search movies", testSearchMovies)
	t.Run("search movies with error", testSearchMoviesWithError)
	t.Run("get movie", testGetMovie)
	t.Run("get movie with error", testGetMovieWithError)
	t.Run("get movies", testGetMovies)
	t.Run("get movies with error", testGetMoviesWithError)
	t.Run("create movie", testCreateMovie)
	t.Run("create movie with error", testCreateMovieWithError)
	t.Run("delete movie", testDeleteMovie)
	t.Run("delete movie with error", testDeleteMovieWithError)
}

var _ usecase.SearchMoviePort = (*fake)(nil)
var _ usecase.SearchMoviesPort = (*fake)(nil)

var _ usecase.SearchMovieCreatePort = (*fake)(nil)
var _ usecase.GetMoviePort = (*fake)(nil)
var _ usecase.GetMoviesPort = (*fake)(nil)
var _ usecase.CreateMoviePort = (*fake)(nil)
var _ usecase.GetImagePort = (*fake)(nil)
var _ usecase.DeleteMoviePort = (*fake)(nil)

type fake struct {
	hasError bool
}

func (f fake) SearchMovie(id uint64) (models.MovieSearchDetails, error) {
	fmt.Println("hello from search fake movie api")
	if f.hasError {
		return models.MovieSearchDetails{}, fmt.Errorf("search movie returns error")
	}
	return expectedMovieSearchDetails, nil
}

func (f fake) SearchMovies(movieName string, page int) (models.MoviesResponseMovieDb, error) {
	if f.hasError {
		return models.MoviesResponseMovieDb{}, fmt.Errorf("search movies returns error")
	}
	return expectedMoviesResponse, nil
}

func (f fake) GetCredits(id uint64) (models.Credits, error) {
	if f.hasError {
		return models.Credits{}, fmt.Errorf("get credits error")
	}
	return credits, nil

}
func (f fake) GetPeopleDetails(id uint64) (models.Person, error) {
	if f.hasError {
		return models.Person{}, fmt.Errorf("get person error")
	}
	return person, nil
}

func (f fake) GetMovie(id uint64) (*models.MovieDetails, error) {
	if f.hasError {
		return &models.MovieDetails{}, fmt.Errorf("get movie returns error")
	}
	return &expectedMovieDetails, nil
}

func (f fake) GetMovies(queryParams models.MovieSearchParameter) ([]models.MovieOverview, error) {
	if f.hasError {
		return []models.MovieOverview{}, fmt.Errorf("get movie returns error")
	}
	return []models.MovieOverview{*expectedMovieOverview.Cast()}, nil
}

func (f fake) CreateMovie(movie *models.MovieDetails) error {
	if f.hasError {
		return fmt.Errorf("get movie returns error")
	}
	return nil
}

func (f fake) CreateGenre(genres []*models.Genre) error {
	if f.hasError {
		return fmt.Errorf("get movie returns error")
	}
	return nil
}

func (f fake) CreateGenreMovieConnection(id uint64, genres []*models.Genre) error {
	if f.hasError {
		return fmt.Errorf("get movie returns error")
	}
	return nil
}

func (f fake) CreateCast(id uint64, cast []models.CastDetails) error {
	if f.hasError {
		return fmt.Errorf("get movie returns error")
	}
	return nil
}

func (f fake) CreatePerson(person *models.Person) error {
	if f.hasError {
		return fmt.Errorf("get movie returns error")
	}
	return nil
}

func (f fake) GetImage(url string) (string, error) {
	if f.hasError {
		return "", fmt.Errorf("get image returns error")
	}
	return "base64image", nil
}

func (f fake) DeleteMovie(id uint64) error {
	if f.hasError {
		return fmt.Errorf("delete movie returns error")
	}
	return nil
}

func testSearchMovie(t *testing.T) {
	movie := models.MovieSearchDetails{}

	fakeApi := fake{}
	u := usecase.NewSearchMovie(fakeApi)

	r, _ := http.NewRequest("GET", "api/v0/search/movie/1234", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "1234"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeSearchMovie(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal(body, &movie)

	assert.Equal(t, http.StatusOK, response.Code)
	assert.Equal(t, expectedMovieSearchDetails, movie)
}

func testSearchMovieWithError(t *testing.T) {
	var errResp errResponse

	fakeApi := fake{hasError: true}
	u := usecase.NewSearchMovie(fakeApi)
	r, _ := http.NewRequest("GET", "api/v0/search/movie/1", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeSearchMovie(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal(body, &errResp)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
	assert.Equal(t, "couldn't get movie with id 1: search movie returns error", errResp.Error)
}

func testSearchMovies(t *testing.T) {
	moviesResponse := models.MoviesResponse{}

	fakeApi := fake{}
	u := usecase.NewSearchMovies(fakeApi)

	r, _ := http.NewRequest("GET", "api/v0/search/movies?MovieName=hello&page=1&size=1", nil)

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeSearchMovies(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal(body, &moviesResponse)

	assert.Equal(t, http.StatusOK, response.Code)
	assert.Equal(t, expectedMoviesResponse.Cast(), moviesResponse)
}

func testSearchMoviesWithError(t *testing.T) {
	var errResp errResponse

	fakeApi := fake{hasError: true}
	u := usecase.NewSearchMovies(fakeApi)

	r, _ := http.NewRequest("GET", "api/v0/search/movies?&page=1&movieName=spiderman", nil)
	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeSearchMovies(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal(body, &errResp)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
	assert.Equal(t, "couldn't get movies with name spiderman: search movies returns error", errResp.Error)
}

func testGetMovie(t *testing.T) {
	movie := models.MovieDetails{}

	fakeApi := fake{}
	u := usecase.NewGetMovie(fakeApi)

	r, _ := http.NewRequest("GET", "api/v0/movie/1", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeGetMovie(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal(body, &movie)

	assert.Equal(t, http.StatusOK, response.Code)
	assert.Equal(t, expectedMovieDetails, movie)
}

func testGetMovieWithError(t *testing.T) {
	var errResp errResponse

	fakeApi := fake{hasError: true}
	u := usecase.NewGetMovie(fakeApi)

	r, _ := http.NewRequest("GET", "api/v0/movie/0", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeGetMovie(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal(body, &errResp)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
	assert.Equal(t, "couldn't get movie with id 1: get movie returns error", errResp.Error)
}

// func testGetMovieWithTypeCastError(t *testing.T) {
// var errResp errResponse

// fakeApi := fake{hasError: true}
// u := usecase.NewGetMovie(fakeApi)

// r, _ := http.NewRequest("GET", "api/v0/movie/1", nil)
// r = mux.SetURLVars(r, map[string]string{"id": "1"})

// response := httptest.NewRecorder()
// adapter := NewAdapter()
// handler := http.HandlerFunc(adapter.MakeGetMovie(u))
// handler.ServeHTTP(response, r)

// body, _ := ioutil.ReadAll(response.Body)
// json.Unmarshal(body, &errResp)

// assert.Equal(t, http.StatusInternalServerError, response.Code)
// assert.Equal(t, `couldn't convert id a in integer: strconv.Atoi: parsing "a": invalid syntax`, errResp.Error)
// }

func testGetMovies(t *testing.T) {
	movie := []models.MovieOverview{}

	fakeApi := fake{}
	u := usecase.NewGetMovies(fakeApi)

	r, _ := http.NewRequest("GET", "api/v0/movies", nil)

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeGetMovies(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal(body, &movie)

	assert.Equal(t, http.StatusOK, response.Code)
	assert.Equal(t, []models.MovieOverview{*expectedMovieOverview.Cast()}, movie)
}

func testGetMoviesWithError(t *testing.T) {
	var errResp errResponse

	fakeApi := fake{hasError: true}
	u := usecase.NewGetMovies(fakeApi)

	r, _ := http.NewRequest("GET", "api/v0/movies", nil)

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeGetMovies(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal(body, &errResp)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
	assert.Equal(t, "couldn't get movies: get movie returns error", errResp.Error)
}

func testCreateMovie(t *testing.T) {
	fakeApi := fake{}
	uCreateMovie := usecase.NewCreateMovie(fakeApi, fakeApi)
	uImage := usecase.NewGetImage(fakeApi)

	r, _ := http.NewRequest("POST", "api/v0/movie/1", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeCreateMovie(uCreateMovie, uImage))
	handler.ServeHTTP(response, r)

	assert.Equal(t, http.StatusCreated, response.Code)
}

func testCreateMovieWithError(t *testing.T) {
	var errResp errResponse

	fakeApi := fake{hasError: true}
	uCreateMovie := usecase.NewCreateMovie(fakeApi, fakeApi)
	uImage := usecase.NewGetImage(fakeApi)

	r, _ := http.NewRequest("POST", "api/v0/movie/1", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeCreateMovie(uCreateMovie, uImage))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal(body, &errResp)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
	assert.Equal(t, `couldn't create movie: couldn't convert to base64 image: get image returns error`, errResp.Error)
}

func testDeleteMovie(t *testing.T) {
	fakeApi := fake{}
	u := usecase.NewDeleteMovie(fakeApi)
	r, _ := http.NewRequest("DELETE", "api/v0/movie/1/", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeDeleteMovie(u))
	handler.ServeHTTP(response, r)

	assert.Equal(t, http.StatusAccepted, response.Code)
}

func testDeleteMovieWithError(t *testing.T) {
	var errResp errResponse

	fakeApi := fake{hasError: true}
	u := usecase.NewDeleteMovie(fakeApi)

	r, _ := http.NewRequest("DELETE", "api/v0/movie/1/", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeDeleteMovie(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal(body, &errResp)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
	assert.Equal(t, "couldn't get movie with id 1: delete movie returns error", errResp.Error)
}
