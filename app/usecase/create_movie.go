package usecase

import (
	"encoding/json"
	"fmt"
	"sync"

	"gitlab.com/thesilk/privlib/app/models"
)

type CreateMoviePort interface {
	CreateMovie(movie *models.MovieDetails) error
	CreateGenre(genres []*models.Genre) error
	CreateGenreMovieConnection(movieID uint64, genres []*models.Genre) error
	CreateCast(movieID uint64, cast []models.CastDetails) error
	CreatePerson(person *models.Person) error
}

type SearchMovieCreatePort interface {
	SearchMovie(id uint64) (models.MovieSearchDetails, error)
	GetCredits(id uint64) (models.Credits, error)
	GetPeopleDetails(id uint64) (models.Person, error)
}

type CreateMovie struct {
	repository CreateMoviePort
	movieApi   SearchMovieCreatePort
}

func NewCreateMovie(repository CreateMoviePort, movieApi SearchMovieCreatePort) CreateMovie {
	return CreateMovie{repository: repository, movieApi: movieApi}
}

func (u CreateMovie) Run(movieID uint64, getImage GetImage) error {
	movie, _ := u.movieApi.SearchMovie(movieID)

	movieDetails, err := u.CastToMovieDetails(&movie)
	if err != nil {
		return fmt.Errorf("couldn't serialize MovieSearchDetails: %v", err)
	}

	image64, err := getImage.Run(movie.PosterPath)
	if err != nil {
		return err
	}

	movieDetails.Image = image64
	if err := u.repository.CreateMovie(movieDetails); err != nil {
		return fmt.Errorf("couldn't store movie '%s': %v", movie.Title, err)
	}

	if err := u.CreateGenre(movieDetails.ID, movieDetails.Genres); err != nil {
		return fmt.Errorf("couldn't create genres: %v", err)
	}

	cast, err := u.CreateCast(movieDetails.ID)
	if err != nil {
		return fmt.Errorf("couldn't create cast: %v", err)
	}

	var (
		wg sync.WaitGroup
	)
	errChan := make(chan error)
	go func() {
		wg.Wait()
		close(errChan)
	}()

	for idx, person := range cast {
		wg.Add(1)
		go u.CreatePerson(&wg, person.ID, getImage, errChan)
		if idx > 8 {
			break
		}
	}

	for err := range errChan {
		if err != nil {
			return fmt.Errorf("couldn't create persons: %v", err)
		}
	}

	return nil
}

func (u CreateMovie) CastToMovieDetails(movie *models.MovieSearchDetails) (*models.MovieDetails, error) {
	movieSerialized, err := json.Marshal(movie)
	if err != nil {
		return &models.MovieDetails{}, fmt.Errorf("couldn't serialize MovieSearchDetails: %v", err)
	}
	movieDetails := new(models.MovieDetails)
	if err := json.Unmarshal(movieSerialized, movieDetails); err != nil {
		return &models.MovieDetails{}, fmt.Errorf("couldn't deserialize MovieSearchDetails to MovieDetails: %v", err)
	}
	return movieDetails, nil
}

func (u CreateMovie) CreateGenre(id uint64, genres []*models.Genre) error {
	if err := u.repository.CreateGenre(genres); err != nil {
		return fmt.Errorf("couldn't create genres: %v", err)
	}

	if err := u.repository.CreateGenreMovieConnection(id, genres); err != nil {
		return fmt.Errorf("couldn't create movie gencre connection: %v", err)
	}
	return nil
}

func (u CreateMovie) CreateCast(id uint64) ([]models.CastDetails, error) {
	credits, err := u.movieApi.GetCredits(id)
	if err != nil {
		return []models.CastDetails{}, fmt.Errorf("couldn't get movie cast: %v", err)
	}

	return credits.Cast, u.repository.CreateCast(id, credits.Cast)
}

func (u CreateMovie) CreatePerson(wg *sync.WaitGroup, id uint64, getImage GetImage, urlChan chan<- error) {
	defer wg.Done()

	person, err := u.movieApi.GetPeopleDetails(id)
	if err != nil {
		urlChan <- fmt.Errorf("couldn't get person: %v", err)
	}

	image64, err := getImage.Run(models.MoviePosterServer + person.ProfilePath)
	if err != nil {
		urlChan <- err
	}
	person.Image = image64

	urlChan <- u.repository.CreatePerson(&person)
}
