package usecase

import "gitlab.com/thesilk/privlib/app/models"

type SearchMoviePort interface {
	SearchMovie(id uint64) (models.MovieSearchDetails, error)
}

type SearchMovie struct {
	movieApi SearchMoviePort
}

func NewSearchMovie(movieApi SearchMoviePort) SearchMovie {
	return SearchMovie{movieApi: movieApi}
}

func (u SearchMovie) Run(id uint64) (models.MovieSearchDetails, error) {
	return u.movieApi.SearchMovie(id)
}
