package usecase

import (
	"gitlab.com/thesilk/privlib/app/models"
)

type SearchMoviesPort interface {
	SearchMovies(movieName string, page int) (models.MoviesResponseMovieDb, error)
}

type SearchMovies struct {
	movieApi SearchMoviesPort
}

func NewSearchMovies(movieApi SearchMoviesPort) SearchMovies {
	return SearchMovies{movieApi: movieApi}
}

func (u SearchMovies) Run(movieName string, page int) (models.MoviesResponse, error) {
	var castedResponse models.MoviesResponse

	if page == 0 {
		page = 1
	}

	response, err := u.movieApi.SearchMovies(movieName, page)
	if err != nil {
		return castedResponse, err
	}

	return response.Cast(), nil
}
