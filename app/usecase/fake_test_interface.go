package usecase

import (
	"fmt"

	"gitlab.com/thesilk/privlib/app/models"
)

var expectedMovieOverviewMovieDb models.MovieOverviewMovieDb = models.MovieOverviewMovieDb{
	ID:         1,
	Title:      "test movie",
	PosterPath: "/abcdef.jpg",
}

var expectedMovieOverview models.MovieOverview = models.MovieOverview{
	ID:         1,
	Title:      "test movie",
	PosterPath: "/abcdef.jpg",
}

var expectedMoviesResponse models.MoviesResponseMovieDb = models.MoviesResponseMovieDb{
	Page:         1,
	TotalResults: 1,
	TotalPages:   1,
	Results:      []*models.MovieOverviewMovieDb{&expectedMovieOverviewMovieDb},
}

var genre models.Genre = models.Genre{ID: 1, Name: "action"}
var cast models.CastDetails = models.CastDetails{
	Character: "peter",
	Gender:    0,
	ID:        1,
	Name:      "pan",
	Order:     1,
}

var credits models.Credits = models.Credits{
	ID:   1,
	Cast: []models.CastDetails{cast},
}

var person models.Person = models.Person{
	ID:    1,
	Name:  "Peter",
	Image: "base64",
}

var expectedMovieDetails models.MovieDetails = models.MovieDetails{
	ID:            1,
	Budget:        1234,
	Homepage:      "http://test.com",
	Title:         "title",
	OriginalTitle: "title",
	Runtime:       111,
	VoteAverage:   3.89,
	VoteCount:     125482,
	Revenue:       123456789,
	RelaseDate:    "01.01.1970",
	Popularity:    4.265,
	Overview:      "here is the plot",
	Image:         "base64",
	Genres:        []*models.Genre{&genre},
	Cast:          []models.CastDetails{cast},
}

var expectedMovieSearchDetails models.MovieSearchDetails = models.MovieSearchDetails{
	ID:            1,
	Budget:        1234,
	Homepage:      "http://test.com",
	Title:         "title",
	OriginalTitle: "title",
	Runtime:       111,
	VoteAverage:   3.89,
	VoteCount:     125482,
	Revenue:       123456789,
	RelaseDate:    "01.01.1970",
	Popularity:    4.265,
	Overview:      "here is the plot",
	Genres:        []models.Genre{genre},
}

var _ SearchMoviePort = (*fake)(nil)
var _ SearchMoviesPort = (*fake)(nil)
var _ SearchMovieCreatePort = (*fake)(nil)
var _ GetMoviePort = (*fake)(nil)
var _ GetMoviesPort = (*fake)(nil)
var _ CreateMoviePort = (*fake)(nil)
var _ GetImagePort = (*fake)(nil)
var _ DeleteMoviePort = (*fake)(nil)

type fake struct {
	hasError bool
}

func (f fake) SearchMovies(movieName string, page int) (models.MoviesResponseMovieDb, error) {
	var response models.MoviesResponseMovieDb

	if f.hasError {
		return response, fmt.Errorf("search movies returns error")
	}
	if page == 1 {
		return expectedMoviesResponse, nil
	} else {
		return models.MoviesResponseMovieDb{TotalResults: 21, TotalPages: 2, Page: 1, StatusMessage: "no movies found", StatusCode: 404}, nil
	}
}

func (f fake) SearchMovie(id uint64) (models.MovieSearchDetails, error) {
	if f.hasError {
		return models.MovieSearchDetails{}, fmt.Errorf("search movie returns error")
	}
	return expectedMovieSearchDetails, nil
}

func (f fake) GetCredits(id uint64) (models.Credits, error) {
	if f.hasError {
		return models.Credits{}, fmt.Errorf("get credits error")
	}
	return credits, nil

}
func (f fake) GetPeopleDetails(id uint64) (models.Person, error) {
	if f.hasError {
		return models.Person{}, fmt.Errorf("get person error")
	}
	return person, nil
}

func (f fake) GetMovie(id uint64) (*models.MovieDetails, error) {
	if f.hasError {
		return &models.MovieDetails{}, fmt.Errorf("get movie returns error")
	}
	return &expectedMovieDetails, nil
}

func (f fake) GetMovies(params models.MovieSearchParameter) ([]models.MovieOverview, error) {
	if f.hasError {
		return []models.MovieOverview{}, fmt.Errorf("get movies returns error")
	}
	return []models.MovieOverview{expectedMovieOverview}, nil
}

func (f fake) CreateMovie(movie *models.MovieDetails) error {
	if f.hasError {
		return fmt.Errorf("get movie returns error")
	}
	return nil
}

func (f fake) CreateGenre(genres []*models.Genre) error {
	if f.hasError {
		return fmt.Errorf("get movie returns error")
	}
	return nil
}

func (f fake) CreateGenreMovieConnection(id uint64, genres []*models.Genre) error {
	if f.hasError {
		return fmt.Errorf("get movie returns error")
	}
	return nil
}

func (f fake) CreateCast(id uint64, cast []models.CastDetails) error {
	if f.hasError {
		return fmt.Errorf("get movie returns error")
	}
	return nil
}

func (f fake) CreatePerson(person *models.Person) error {
	if f.hasError {
		return fmt.Errorf("get movie returns error")
	}
	return nil
}

func (f fake) GetImage(url string) (string, error) {
	if f.hasError {
		return "", fmt.Errorf("get image returns error")
	}
	return "base64image", nil
}

func (f fake) DeleteMovie(id uint64) error {
	if f.hasError {
		return fmt.Errorf("delete movie returns error")
	}
	return nil
}
