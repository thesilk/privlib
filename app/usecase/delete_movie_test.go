package usecase

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDeleteMovieRun(t *testing.T) {
	fakeApi := fake{}
	u := NewDeleteMovie(fakeApi)

	err := u.Run(1)
	assert.Nil(t, err)
}

func TestDeleteMovieRunError(t *testing.T) {
	fakeApi := fake{hasError: true}
	u := NewDeleteMovie(fakeApi)

	err := u.Run(1)
	assert.Equal(t, "delete movie returns error", err.Error())
}
