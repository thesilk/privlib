package usecase

type DeleteMoviePort interface {
	DeleteMovie(id uint64) error
}

type DeleteMovie struct {
	repository DeleteMoviePort
}

func NewDeleteMovie(repository DeleteMoviePort) DeleteMovie {
	return DeleteMovie{repository: repository}
}

func (u DeleteMovie) Run(id uint64) error {
	return u.repository.DeleteMovie(id)
}
