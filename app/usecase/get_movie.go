package usecase

import (
	"gitlab.com/thesilk/privlib/app/models"
)

type GetMoviePort interface {
	GetMovie(id uint64) (*models.MovieDetails, error)
}

type GetMovie struct {
	repository GetMoviePort
}

func NewGetMovie(repository GetMoviePort) GetMovie {
	return GetMovie{repository: repository}
}

func (u GetMovie) Run(id uint64) (*models.MovieDetails, error) {
	return u.repository.GetMovie(id)
}
