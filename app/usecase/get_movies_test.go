package usecase

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/thesilk/privlib/app/models"
)

func TestGetMoviesRun(t *testing.T) {
	fakeApi := fake{}
	u := NewGetMovies(fakeApi)

	params := models.MovieSearchParameter{Page: 1, Size: 12}

	movies, err := u.Run(params)
	assert.Nil(t, err)
	assert.Equal(t, []models.MovieOverview{expectedMovieOverview}, movies)
}

func TestGetMoviesRunError(t *testing.T) {
	fakeApi := fake{hasError: true}
	u := NewGetMovies(fakeApi)

	params := models.MovieSearchParameter{Page: 1, Size: 12}

	movies, err := u.Run(params)
	assert.Equal(t, "get movies returns error", err.Error())
	assert.Equal(t, []models.MovieOverview{}, movies)
}
