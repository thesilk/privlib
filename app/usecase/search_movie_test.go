package usecase

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/thesilk/privlib/app/models"
)

func TestSearchMovieRun(t *testing.T) {
	fakeApi := fake{}
	u := NewSearchMovie(fakeApi)

	response, err := u.Run(1)

	assert.Equal(t, expectedMovieSearchDetails, response)
	assert.Nil(t, err)
}

func TestSearchMovieRunError(t *testing.T) {
	fakeApi := fake{hasError: true}
	u := NewSearchMovie(fakeApi)

	response, err := u.Run(1)

	assert.Equal(t, models.MovieSearchDetails{}, response)
	assert.Equal(t, "search movie returns error", err.Error())
}
