package usecase

import (
	"gitlab.com/thesilk/privlib/app/models"
)

type GetMoviesPort interface {
	GetMovies(queryParams models.MovieSearchParameter) ([]models.MovieOverview, error)
}

type GetMovies struct {
	repository GetMoviesPort
}

func NewGetMovies(repository GetMoviesPort) GetMovies {
	return GetMovies{repository: repository}
}

func (u GetMovies) Run(queryParams models.MovieSearchParameter) ([]models.MovieOverview, error) {
	return u.repository.GetMovies(queryParams)
}
