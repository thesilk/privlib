package usecase

import (
	"fmt"
)

type GetImagePort interface {
	GetImage(url string) (string, error)
}

type GetImage struct {
	image GetImagePort
}

func NewGetImage(image GetImagePort) GetImage {
	return GetImage{image: image}
}

func (u GetImage) Run(url string) (string, error) {
	image64, err := u.image.GetImage(url)
	if err != nil {
		return "", fmt.Errorf("couldn't convert to base64 image: %v", err)
	}
	return image64, nil
}
