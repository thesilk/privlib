package usecase

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreateMovieRun(t *testing.T) {
	fakeApi := fake{}
	uCreateMovie := NewCreateMovie(fakeApi, fakeApi)
	uImage := NewGetImage(fakeApi)

	err := uCreateMovie.Run(1, uImage)
	assert.Nil(t, err)
}

func TestCreateMovieRunError(t *testing.T) {
	fakeApi := fake{hasError: true}
	uCreateMovie := NewCreateMovie(fakeApi, fakeApi)
	uImage := NewGetImage(fakeApi)

	err := uCreateMovie.Run(1, uImage)
	assert.Equal(t, "couldn't convert to base64 image: get image returns error", err.Error())
}
