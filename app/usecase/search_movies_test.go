package usecase

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/thesilk/privlib/app/models"
)

func TestSearchMoviesRun(t *testing.T) {
	fakeApi := fake{}
	u := NewSearchMovies(fakeApi)

	response, err := u.Run("test movie", 1)

	assert.Equal(t, expectedMoviesResponse.Cast(), response)
	assert.Nil(t, err)
}

func TestSearchMoviesRunError(t *testing.T) {
	fakeApi := fake{hasError: true}
	u := NewSearchMovies(fakeApi)

	response, err := u.Run("test movie", 1)

	assert.Equal(t, models.MoviesResponse{}, response)
	assert.Equal(t, "search movies returns error", err.Error())
}
