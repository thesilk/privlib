package usecase

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/thesilk/privlib/app/models"
)

func TestGetMovieRun(t *testing.T) {
	fakeApi := fake{}
	u := NewGetMovie(fakeApi)

	movie, err := u.Run(1)
	assert.Nil(t, err)
	assert.Equal(t, &expectedMovieDetails, movie)
}

func TestGetMovieRunError(t *testing.T) {
	fakeApi := fake{hasError: true}
	u := NewGetMovie(fakeApi)

	movie, err := u.Run(1)
	assert.Equal(t, "get movie returns error", err.Error())
	assert.Equal(t, &models.MovieDetails{}, movie)
}
