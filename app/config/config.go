package config

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sync"

	yaml "gopkg.in/yaml.v2"
)

type config struct {
	ServerPort int    `yaml:"server_port"`
	ServerHost string `yaml:"server_host"`
	ApiKey     string `yaml:"api_key"`
}

var (
	Cfg   *config
	mutex sync.Mutex
)

func UpdateConfig() bool {
	var needUpdate bool

	if Cfg == nil {
		Cfg = &config{
			ServerHost: "localhost",
			ServerPort: 2342,
		}
		log.Println("missing api key ...")
		needUpdate = true
	}

	return needUpdate
}

func InitConfig(configPath string) error {
	if _, err := os.Stat(configPath); !os.IsNotExist(err) {
		if err := readConfig(configPath); err != nil {
			return err
		}
		if UpdateConfig() {
			return writeConfig(configPath)
		}
	} else {
		UpdateConfig()
		return writeConfig(configPath)
	}

	return nil
}

func readConfig(configPath string) error {
	f, err := ioutil.ReadFile(configPath)
	if err != nil {
		return fmt.Errorf("couldn't open config.yaml: %v", err)
	}

	err = yaml.Unmarshal(f, &Cfg)
	if err != nil {
		return fmt.Errorf("couldn't read config.yaml: %v", err)
	}
	return nil
}

func writeConfig(configPath string) error {
	cfg, err := yaml.Marshal(&Cfg)
	if err != nil {
		return fmt.Errorf("couldn't marshal config: %v", err)
	}

	mutex.Lock()

	if err := ioutil.WriteFile(configPath, cfg, 0644); err != nil {
		mutex.Unlock()
		return fmt.Errorf("couldn't write config file: %v", err)
	}

	mutex.Unlock()

	return nil
}

func (c *config) GetServerPort() int {
	return c.ServerPort
}

func (c *config) SetServerPort(port int) {
	c.ServerPort = port
}

func (c *config) GetServerHost() string {
	return c.ServerHost
}

func (c *config) SetServerHost(host string) {
	c.ServerHost = host
}

func (c *config) GetAPIKey() string {
	return c.ApiKey
}

func (c *config) SetAPIKey(key string) {
	c.ApiKey = key
}
