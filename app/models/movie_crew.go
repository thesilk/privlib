package models

type CrewDetailsImdb struct {
	CreditID    string `json:"creditId"`
	Department  string `json:"department"`
	Job         string `json:"job"`
	Gender      uint8  `json:"gender"`
	ID          uint64 `json:"id"`
	Name        string `json:"name"`
	ProfilePath string `json:"profilePath"`
}

func (c *CrewDetailsImdb) Cast() CrewDetails {
	var crewDetails CrewDetails

	crewDetails.CreditID = c.CreditID
	crewDetails.Department = c.Department
	crewDetails.Job = c.Job
	crewDetails.Gender = c.Gender
	crewDetails.ID = c.ID
	crewDetails.Name = c.Name
	crewDetails.ProfilePath = c.ProfilePath

	return crewDetails
}

type CrewDetails struct {
	CreditID    string `json:"creditId"`
	Department  string `json:"department"`
	Job         string `json:"job"`
	Gender      uint8  `json:"gender"`
	ID          uint64 `json:"id"`
	Name        string `json:"name"`
	ProfilePath string `json:"profilePath"`
}
