package models

//Movie contains imdb information
type MovieDetails struct {
	ID            uint64        `json:"id"`
	Budget        uint64        `json:"budget"`
	Homepage      string        `json:"homepage"`
	Title         string        `json:"title"`
	OriginalTitle string        `json:"originalTitle"`
	Runtime       uint16        `json:"runtime,omitempty"`
	VoteAverage   float64       `json:"voteAverage"`
	VoteCount     uint64        `json:"voteCount"`
	Revenue       uint64        `json:"revenue"`
	RelaseDate    string        `json:"releaseDate"`
	Popularity    float64       `json:"popularity"`
	Overview      string        `json:"overview"`
	Image         string        `json:"image,omitempty"`
	Genres        []*Genre      `json:"genres"`
	Cast          []CastDetails `json:"cast"`
	Crew          []CrewDetails `json:"crew"`
}
