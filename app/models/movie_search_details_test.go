package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCastMovieDetails(t *testing.T) {
	movieImdb := MovieSearchDetailsImdb{
		ID:         1,
		Title:      "title",
		Popularity: 9.99,
		Homepage:   "Homepage",
		PosterPath: "/url",
	}

	expectedMovie := MovieSearchDetails{
		ID:         1,
		Title:      "title",
		Popularity: 9.99,
		Homepage:   "Homepage",
		PosterPath: "https://image.tmdb.org/t/p/w300/url",
	}

	assert.Equal(t, movieImdb.Cast(), expectedMovie)
}
