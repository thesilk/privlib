package models

type CreditsImdb struct {
	ID            uint64            `json:"id"`
	CastDetails   []CastDetailsImdb `json:"cast"`
	CrewDetails   []CrewDetailsImdb `json:"crew"`
	StatusMessage string            `json:"status_message,omitempty"`
	StatusCode    uint16            `json:"status_code,omitempty"`
}

func (c *CreditsImdb) Cast() Credits {
	var (
		cast    []CastDetails
		crew    []CrewDetails
		credits Credits
	)

	for _, r := range c.CastDetails {
		cast = append(cast, r.Cast())
	}

	for _, r := range c.CrewDetails {
		crew = append(crew, r.Cast())
	}

	credits.ID = c.ID
	credits.Cast = cast
	credits.Crew = crew
	credits.StatusCode = c.StatusCode
	credits.StatusMessage = c.StatusMessage

	return credits
}

type Credits struct {
	ID            uint64        `json:"id"`
	Cast          []CastDetails `json:"cast"`
	Crew          []CrewDetails `json:"crew"`
	StatusMessage string        `json:"statusMessage,omitempty"`
	StatusCode    uint16        `json:"statusCode,omitempty"`
}
