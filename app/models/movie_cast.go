package models

type CastDetailsImdb struct {
	CastID      uint64 `json:"cast_id"`
	Character   string `json:"character"`
	CreditId    string `json:"credit_id"`
	Gender      uint8  `json:"gender"`
	ID          uint64 `json:"id"`
	Name        string `json:"name"`
	Order       uint16 `json:"order"`
	ProfilePath string `json:"profile_path"`
	Image       string `json:"image,omitempty"`
}

func (c *CastDetailsImdb) Cast() CastDetails {
	var castDetails CastDetails
	castDetails.CastID = c.CastID
	castDetails.Character = c.Character
	castDetails.CreditId = c.CreditId
	castDetails.Gender = c.Gender
	castDetails.ID = c.ID
	castDetails.Name = c.Name
	castDetails.Order = c.Order
	castDetails.ProfilePath = c.ProfilePath
	castDetails.Image = c.Image

	return castDetails
}

type CastDetails struct {
	CastID      uint64 `json:"castId"`
	Character   string `json:"character"`
	CreditId    string `json:"creditId"`
	Gender      uint8  `json:"gender"`
	ID          uint64 `json:"id"`
	Name        string `json:"name"`
	Order       uint16 `json:"order"`
	ProfilePath string `json:"profilePath"`
	Image       string `json:"image,omitempty"`
}
