package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCastMovieParameters(t *testing.T) {
	parameters := NewMovieSearchParameter()

	expectedParameters := MovieSearchParameter{
		Page: 1,
		Size: 20,
	}

	assert.Equal(t, parameters, expectedParameters)
}
