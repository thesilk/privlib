package models

type Genre struct {
	ID   uint16 `json:"id"`
	Name string `json:"name"`
}
