package models

//Movie contains imdb information
type MovieSearchDetailsImdb struct {
	ID            uint64  `json:"id"`
	Budget        uint64  `json:"budget"`
	Genres        []Genre `json:"genres"`
	Homepage      string  `json:"homepage"`
	Title         string  `json:"title"`
	OriginalTitle string  `json:"original_title"`
	Runtime       uint16  `json:"runtime,omitempty"`
	VoteAverage   float64 `json:"vote_average"`
	VoteCount     uint64  `json:"vote_count"`
	Revenue       uint64  `json:"revenue"`
	RelaseDate    string  `json:"release_date"`
	PosterPath    string  `json:"poster_path"`
	Popularity    float64 `json:"popularity"`
	Overview      string  `json:"overview"`
	StatusMessage string  `json:"status_message,omitempty"`
	StatusCode    uint16  `json:"status_code,omitempty"`
}

func (m *MovieSearchDetailsImdb) Cast() MovieSearchDetails {
	var movieSearchDetails MovieSearchDetails

	movieSearchDetails.ID = m.ID
	movieSearchDetails.Budget = m.Budget
	movieSearchDetails.Genres = m.Genres
	movieSearchDetails.Homepage = m.Homepage
	movieSearchDetails.Title = m.Title
	movieSearchDetails.OriginalTitle = m.OriginalTitle
	movieSearchDetails.Runtime = m.Runtime
	movieSearchDetails.VoteAverage = m.VoteAverage
	movieSearchDetails.VoteCount = m.VoteCount
	movieSearchDetails.Revenue = m.Revenue
	movieSearchDetails.RelaseDate = m.RelaseDate
	movieSearchDetails.PosterPath = MoviePosterServer + m.PosterPath
	movieSearchDetails.Popularity = m.Popularity
	movieSearchDetails.Overview = m.Overview
	movieSearchDetails.StatusCode = m.StatusCode
	movieSearchDetails.StatusMessage = m.StatusMessage

	return movieSearchDetails
}

type MovieSearchDetails struct {
	ID            uint64  `json:"id"`
	Budget        uint64  `json:"budget"`
	Genres        []Genre `json:"genres"`
	Homepage      string  `json:"homepage"`
	Title         string  `json:"title"`
	OriginalTitle string  `json:"originalTitle"`
	Runtime       uint16  `json:"runtime,omitempty"`
	VoteAverage   float64 `json:"voteAverage"`
	VoteCount     uint64  `json:"voteCount"`
	Revenue       uint64  `json:"revenue"`
	RelaseDate    string  `json:"releaseDate"`
	PosterPath    string  `json:"posterPath"`
	Popularity    float64 `json:"popularity"`
	Overview      string  `json:"overview"`
	StatusMessage string  `json:"statusMessage,omitempty"`
	StatusCode    uint16  `json:"statusCode,omitempty"`
}
