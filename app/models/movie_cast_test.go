package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCastMovieCast(t *testing.T) {
	castImdb := CastDetailsImdb{
		CastID:      1,
		Character:   "character",
		CreditId:    "1",
		Gender:      2,
		ID:          1,
		Name:        "name",
		Order:       1,
		ProfilePath: "path",
		Image:       "image",
	}

	expectedCast := CastDetails{
		CastID:      1,
		Character:   "character",
		CreditId:    "1",
		Gender:      2,
		ID:          1,
		Name:        "name",
		Order:       1,
		ProfilePath: "path",
		Image:       "image",
	}

	assert.Equal(t, castImdb.Cast(), expectedCast)
}
