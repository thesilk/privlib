package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCastMovieCredits(t *testing.T) {
	castImdb := []CastDetailsImdb{{
		CastID:      1,
		Character:   "character",
		Name:        "name",
		ProfilePath: "path",
	}}

	crewImdb := []CrewDetailsImdb{{
		CreditID:    "1",
		Name:        "name",
		ProfilePath: "path",
	},
	}

	creditImdb := CreditsImdb{
		ID:            1,
		CastDetails:   castImdb,
		CrewDetails:   crewImdb,
		StatusMessage: "status",
		StatusCode:    200,
	}

	expectedCast := []CastDetails{{
		CastID:      1,
		Character:   "character",
		Name:        "name",
		ProfilePath: "path",
	}}

	expectedCrew := []CrewDetails{{
		CreditID:    "1",
		Name:        "name",
		ProfilePath: "path",
	}}

	expectedCredit := Credits{
		ID:            1,
		Cast:          expectedCast,
		Crew:          expectedCrew,
		StatusMessage: "status",
		StatusCode:    200,
	}

	assert.Equal(t, creditImdb.Cast(), expectedCredit)
}
