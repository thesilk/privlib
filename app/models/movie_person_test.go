package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCastMoviePerson(t *testing.T) {
	personImdb := PersonImdb{
		ID:                 1,
		Birthday:           "01.01.1970",
		KnownForDepartment: "department",
		DeathDay:           "01.01.2099",
		Name:               "name",
		Gender:             2,
		Biography:          "biography",
		Popularity:         9.99,
		PlaceOfBirth:       "place_of_birth",
		ProfilePath:        "ProfilePath",
		Image:              "Image",
		Adult:              true,
		Homepage:           "Homepage",
	}

	expectedPerson := Person{
		ID:                 1,
		Birthday:           "01.01.1970",
		KnownForDepartment: "department",
		DeathDay:           "01.01.2099",
		Name:               "name",
		Gender:             2,
		Biography:          "biography",
		Popularity:         9.99,
		PlaceOfBirth:       "place_of_birth",
		ProfilePath:        "ProfilePath",
		Image:              "Image",
		Adult:              true,
		Homepage:           "Homepage",
	}

	assert.Equal(t, personImdb.Cast(), expectedPerson)
}
