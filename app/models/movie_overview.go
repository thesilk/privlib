package models

type MovieOverviewMovieDb struct {
	ID          uint64  `json:"id"`
	PosterPath  string  `json:"poster_path,omitempty"`
	Image       string  `json:"image,omitempty"`
	Title       string  `json:"title"`
	VoteAverage float64 `json:"vote_average"`
}

func (m MovieOverviewMovieDb) Cast() *MovieOverview {
	var movieOverview MovieOverview
	movieOverview.ID = m.ID
	movieOverview.Image = m.Image
	movieOverview.PosterPath = MoviePosterServer + m.PosterPath
	movieOverview.Title = m.Title
	movieOverview.VoteAverage = m.VoteAverage

	return &movieOverview
}

type MovieOverview struct {
	ID          uint64  `json:"id"`
	PosterPath  string  `json:"posterPath,omitempty"`
	Image       string  `json:"image,omitempty"`
	Title       string  `json:"title"`
	VoteAverage float64 `json:"voteAverage"`
}
