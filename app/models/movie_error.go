package models

type ErrorResponse struct {
	StatusMessage string `json:"statusMessage,omitempty"`
	StatusCode    uint16 `json:"statusCode,omitempty"`
}
