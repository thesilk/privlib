package models

type MovieSearchParameter struct {
	MovieName      string `json:"movieName"`
	Page           int    `json:"page"`
	Size           int    `json:"size"`
	RuntimeFrom    uint16 `json:"runtimeFrom"`
	RuntimeTo      uint16 `json:"runtimeTo"`
	RatingFrom     uint8  `json:"ratingFrom"`
	RatingTo       uint8  `json:"ratingTo"`
	YearFrom       uint16 `json:"yearFrom"`
	YearTo         uint16 `json:"yearTo"`
	OrderCategory  string `json:"orderCategory"`
	OrderDirection string `json:"orderDirection"`
	TitleLike      string `json:"titleLike"`
}

func NewMovieSearchParameter() MovieSearchParameter {
	return MovieSearchParameter{
		Page: 1,
		Size: 20,
	}
}
