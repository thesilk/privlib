package models

type PersonImdb struct {
	ID                 uint64   `json:"id"`
	Birthday           string   `json:"birthday"`
	KnownForDepartment string   `json:"known_for_department"`
	DeathDay           string   `json:"deathday"`
	Name               string   `json:"name"`
	AlsoKnownAs        []string `json:"also_known_as"`
	Gender             uint8    `json:"gender"`
	Biography          string   `json:"biography"`
	Popularity         float64  `json:"popularity"`
	PlaceOfBirth       string   `json:"place_of_birth"`
	ProfilePath        string   `json:"profile_path"`
	Image              string   `json:"image,omitempty"`
	Adult              bool     `json:"adult"`
	Homepage           string   `json:"homepage"`
}

func (p *PersonImdb) Cast() Person {
	var person Person
	person.ID = p.ID
	person.Birthday = p.Birthday
	person.KnownForDepartment = p.KnownForDepartment
	person.DeathDay = p.DeathDay
	person.Name = p.Name
	person.AlsoKnownAs = p.AlsoKnownAs
	person.Gender = p.Gender
	person.Biography = p.Biography
	person.Popularity = p.Popularity
	person.PlaceOfBirth = p.PlaceOfBirth
	person.ProfilePath = p.ProfilePath
	person.Image = p.Image
	person.Adult = p.Adult
	person.Homepage = p.Homepage

	return person
}

type Person struct {
	ID                 uint64   `json:"id"`
	Birthday           string   `json:"birthday"`
	KnownForDepartment string   `json:"knownForDepartment"`
	DeathDay           string   `json:"deathDay"`
	Name               string   `json:"name"`
	AlsoKnownAs        []string `json:"alsoKnownAs"`
	Gender             uint8    `json:"gender"`
	Biography          string   `json:"biography"`
	Popularity         float64  `json:"popularity"`
	PlaceOfBirth       string   `json:"placeOfBirth"`
	ProfilePath        string   `json:"profilePath"`
	Image              string   `json:"image,omitempty"`
	Adult              bool     `json:"adult"`
	Homepage           string   `json:"homepage"`
}
