package models

type MoviesResponseMovieDb struct {
	Page          uint8                   `json:"page"`
	Results       []*MovieOverviewMovieDb `json:"results"`
	TotalResults  uint16                  `json:"total_results"`
	TotalPages    uint8                   `json:"total_pages"`
	StatusMessage string                  `json:"status_message,omitempty"`
	StatusCode    uint16                  `json:"status_code,omitempty"`
}

func (m *MoviesResponseMovieDb) Cast() MoviesResponse {
	var (
		moviesOverview []*MovieOverview
		moviesResponse MoviesResponse
	)
	for _, r := range m.Results {
		moviesOverview = append(moviesOverview, r.Cast())
	}

	moviesResponse.TotalResults = m.TotalResults
	moviesResponse.TotalPages = m.TotalPages
	moviesResponse.StatusCode = m.StatusCode
	moviesResponse.StatusMessage = m.StatusMessage
	moviesResponse.Page = m.Page
	moviesResponse.Results = moviesOverview

	return moviesResponse
}

type MoviesResponse struct {
	Page          uint8            `json:"page"`
	Results       []*MovieOverview `json:"results"`
	TotalResults  uint16           `json:"totalResults"`
	TotalPages    uint8            `json:"totalPages"`
	StatusMessage string           `json:"statusMessage,omitempty"`
	StatusCode    uint16           `json:"statusCode,omitempty"`
}

type MovieChan struct {
	Response chan MoviesResponseMovieDb
	Err      chan error
}
