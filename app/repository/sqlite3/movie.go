package sqlite3

import (
	"fmt"

	"gitlab.com/thesilk/privlib/app/models"
)

//GetMovie selects all relevant information to given movie id
func (m *Repository) GetMovie(id uint64) (*models.MovieDetails, error) {
	movie, err := m.GetMovieDetails(id)
	if err != nil {
		return &models.MovieDetails{}, fmt.Errorf("couldn't get movie details with id %d: %v", id, err)
	}

	genres, err := m.GetMovieGenres(id)
	if err != nil {
		return &models.MovieDetails{}, fmt.Errorf("couldn't get movie genres with id %d: %v", id, err)
	}
	movie.Genres = genres

	cast, err := m.GetMovieCast(id)
	if err != nil {
		return &models.MovieDetails{}, fmt.Errorf("couldn't get movie cast with id %d: %v", id, err)
	}
	movie.Cast = cast

	return movie, nil
}

//GetMovieDetails selects movie with given id in struct
func (m *Repository) GetMovieDetails(id uint64) (*models.MovieDetails, error) {
	var movie models.MovieDetails
	err := m.DB.QueryRow("SELECT * FROM movie where id=$1", id).Scan(
		&movie.ID,
		&movie.Title,
		&movie.OriginalTitle,
		&movie.Overview,
		&movie.Budget,
		&movie.Homepage,
		&movie.Runtime,
		&movie.VoteAverage,
		&movie.VoteCount,
		&movie.Revenue,
		&movie.RelaseDate,
		&movie.Popularity,
		&movie.Image,
	)
	if err != nil {
		return &movie, fmt.Errorf("couldn't get movie entry from db: %v", err)
	}

	return &movie, nil
}

//GetMovieGenres returns genres for given movie id
func (m *Repository) GetMovieGenres(id uint64) ([]*models.Genre, error) {
	var genres []*models.Genre
	rows, err := m.DB.Query("SELECT genre_id, genre.name FROM movie_has_genre INNER JOIN genre ON genre.id = movie_has_genre.genre_id WHERE movie_id=$1;", id)
	if err != nil {
		return genres, fmt.Errorf("couldn't get genre entry from db: %v", err)
	}
	defer rows.Close()

	for rows.Next() {
		var genre models.Genre
		err := rows.Scan(
			&genre.ID,
			&genre.Name,
		)
		if err != nil {
			return genres, fmt.Errorf("couldn't scan genre row: %v", err)
		}

		genres = append(genres, &genre)
	}

	return genres, nil
}

//GetMovieCast returns genres for given movie id
func (m *Repository) GetMovieCast(id uint64) ([]models.CastDetails, error) {
	var cast []models.CastDetails
	rows, err := m.DB.Query("SELECT character, person_id, person.name, person.gender, person.image FROM credits_cast INNER JOIN person ON person.id = credits_cast.person_id WHERE movie_id=$1;", id)
	if err != nil {
		return cast, fmt.Errorf("couldn't get cast details entry from db: %v", err)
	}
	defer rows.Close()

	for rows.Next() {
		var castCharacter models.CastDetails
		err := rows.Scan(
			&castCharacter.Character,
			&castCharacter.ID,
			&castCharacter.Name,
			&castCharacter.Gender,
			&castCharacter.Image,
		)
		if err != nil {
			return cast, fmt.Errorf("couldn't scan cast row: %v", err)
		}

		cast = append(cast, castCharacter)
	}

	return cast, nil
}

//CreateMovie creates movie entry in database from struct
func (m *Repository) CreateMovie(movie *models.MovieDetails) error {
	stmt, err := m.DB.Prepare(`
		INSERT INTO
			movie (
				id,
				title,
				original_title,
				release_date,
				vote_average,
				vote_count,
				runtime,
				revenue,
				homepage,
				popularity,
				overview,
				budget,
				image
			) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)`)

	if err != nil {
		return fmt.Errorf("couldn't prepare insert statement: %v", err)
	}

	_, err = stmt.Exec(
		&movie.ID,
		&movie.Title,
		&movie.OriginalTitle,
		&movie.RelaseDate,
		&movie.VoteAverage,
		&movie.VoteCount,
		&movie.Runtime,
		&movie.Revenue,
		&movie.Homepage,
		&movie.Popularity,
		&movie.Overview,
		&movie.Budget,
		&movie.Image)
	if err != nil {
		return fmt.Errorf("couldn't execute movie insert statement: %v", err)
	}
	return nil
}

//CreateGenre creates unique genre entry in database from struct
func (m *Repository) CreateGenre(genres []*models.Genre) error {
	stmt, err := m.DB.Prepare(`
		INSERT OR IGNORE INTO
			genre (
				id,
				name
				) VALUES (?,?)`)

	if err != nil {
		return fmt.Errorf("couldn't prepare insert statement: %v", err)
	}

	for _, genre := range genres {
		_, err = stmt.Exec(
			&genre.ID,
			&genre.Name)
		if err != nil {
			return fmt.Errorf("couldn't execute movie insert statement: %v", err)
		}

	}
	return nil
}

//CreateGenreMovieConnection creates unique genre entry and connect to movie in database from struct
func (m *Repository) CreateGenreMovieConnection(movieID uint64, genres []*models.Genre) error {
	if err := m.CreateGenre(genres); err != nil {
		return err
	}

	stmt, err := m.DB.Prepare(`
		INSERT INTO
			movie_has_genre (
				movie_id,
				genre_id
			) VALUES (?,?)`)

	if err != nil {
		return fmt.Errorf("couldn't prepare insert statement: %v", err)
	}

	for _, genre := range genres {
		_, err = stmt.Exec(
			&movieID,
			&genre.ID)
		if err != nil {
			return fmt.Errorf("couldn't execute movie_has_genre insert statement: %v", err)
		}

	}
	return nil
}

//CreateCast creates cast details for given movie in database from struct
func (m *Repository) CreateCast(movieID uint64, cast []models.CastDetails) error {
	stmt, err := m.DB.Prepare(`
		INSERT INTO
			credits_cast (
				movie_id,
				person_id,
				character,
				'order'
			) VALUES (?,?,?,?)`)

	if err != nil {
		return fmt.Errorf("couldn't prepare insert statement: %v", err)
	}

	for idx, character := range cast {
		_, err = stmt.Exec(
			&movieID,
			&character.ID,
			&character.Character,
			&character.Order,
		)
		if err != nil {
			return fmt.Errorf("couldn't execute movie insert statement: %v", err)
		}

		if idx >= 8 {
			break
		}

	}
	return nil
}

//CreatePerson creates cast details for given movie in database from struct
func (m *Repository) CreatePerson(person *models.Person) error {
	stmt, err := m.DB.Prepare(`
		INSERT OR IGNORE INTO
			person (
				id,
				name,
				birthday,
				place_of_birth,
				death_day,
				gender,
				biography,
				popularity,
				image
			) VALUES (?,?,?,?,?,?,?,?,?)`)

	if err != nil {
		return fmt.Errorf("couldn't prepare insert statement: %v", err)
	}

	_, err = stmt.Exec(
		&person.ID,
		&person.Name,
		&person.Birthday,
		&person.PlaceOfBirth,
		&person.DeathDay,
		&person.Gender,
		&person.Biography,
		&person.Popularity,
		&person.Image,
	)
	if err != nil {
		return fmt.Errorf("couldn't execute person insert statement: %v", err)
	}

	return nil
}

//TODO: UpdateMovie

//DeleteMovie deletes all dependent movie information
func (m *Repository) DeleteMovie(id uint64) error {
	if err := m.DeleteGivenMovie(id); err != nil {
		return fmt.Errorf("couldn't delete movie with id %d: %v", id, err)
	}

	if err := m.DeleteMovieGenreConnection(id); err != nil {
		return fmt.Errorf("couldn't delete movie genre connection with id %d: %v", id, err)
	}

	if err := m.DeleteMovieCrewCast(id); err != nil {
		return fmt.Errorf("couldn't delete movie cast with id %d: %v", id, err)
	}

	return nil
}

//DeleteGivenMovie deletes given movie
func (m *Repository) DeleteGivenMovie(id uint64) error {
	stmt, err := m.DB.Prepare("DELETE FROM movie where id=?")
	if err != nil {
		return fmt.Errorf("couldn't pepare statement for deleting movie with id %d: %v", id, err)
	}

	_, err = stmt.Exec(id)
	if err != nil {
		return fmt.Errorf("couldn't execute query for deleting movie with id %d: %v", id, err)
	}

	return nil
}

//DeleteMovieGenreConnection deletes given movie
func (m *Repository) DeleteMovieGenreConnection(id uint64) error {
	stmt, err := m.DB.Prepare("DELETE FROM movie_has_genre where movie_id=?")
	if err != nil {
		return fmt.Errorf("couldn't pepare statement for deleting movie with id %d: %v", id, err)
	}

	_, err = stmt.Exec(id)
	if err != nil {
		return fmt.Errorf("couldn't execute query for deleting movie_has_genre with id %d: %v", id, err)
	}

	return nil
}

//DeleteMovieCrewCast deletes given movie
func (m *Repository) DeleteMovieCrewCast(id uint64) error {
	stmt, err := m.DB.Prepare("DELETE FROM credits_cast where movie_id=?")
	if err != nil {
		return fmt.Errorf("couldn't pepare statement for deleting movie with id %d: %v", id, err)
	}

	_, err = stmt.Exec(id)
	if err != nil {
		return fmt.Errorf("couldn't execute query for deleting credits_cast with id %d: %v", id, err)
	}

	return nil
}

//GetMovies returns all movies in from movies table
func (m *Repository) GetMovies(params models.MovieSearchParameter) ([]models.MovieOverview, error) {
	movies := []models.MovieOverview{}
	if params.Size == 0 {
		params.Size = 12
	}
	if params.Page == 0 {
		params.Page = 1
	}
	start := ((params.Page - 1) * params.Size)
	rows, err := m.DB.Query("SELECT id, title, vote_average, image FROM movie ORDER BY title LIMIT ?,?", start, params.Size)
	if err != nil {
		return []models.MovieOverview{}, fmt.Errorf("couldn't get movies from db: %v", err)
	}
	defer rows.Close()

	for rows.Next() {
		var movie models.MovieOverview
		err := rows.Scan(
			&movie.ID,
			&movie.Title,
			&movie.VoteAverage,
			&movie.Image)
		if err != nil {
			return []models.MovieOverview{}, fmt.Errorf("couldn't scan movie row: %v", err)
		}

		movies = append(movies, movie)
	}

	return movies, nil
}
