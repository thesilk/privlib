package sqlite3

import (
	"fmt"
	"log"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/thesilk/privlib/app/models"
	"gopkg.in/testfixtures.v2"
)

var genres []*models.Genre = []*models.Genre{
	{
		ID:   1,
		Name: "action",
	},
	{
		ID:   2,
		Name: "adventure",
	},
}

var cast []models.CastDetails = []models.CastDetails{
	{
		Character: "peter",
		Name:      "theSilk",
		Image:     "base64",
		ID:        1,
		Gender:    2,
	},
}

var expectedMovie models.MovieDetails = models.MovieDetails{
	ID:            1,
	Title:         "test movie part 1",
	OriginalTitle: "test movie part 1",
	Overview:      "plot",
	Runtime:       123,
	Budget:        12345,
	RelaseDate:    "01.01.1970",
	Revenue:       12345679,
	Image:         "base64",
	Genres:        genres,
	Cast:          cast,
	Popularity:    5896,
	VoteAverage:   5.89,
	VoteCount:     569865,
	Homepage:      "url123",
}

func prepareTestDatabase() *Repository {
	repo := NewTestRepository("../../migrations")

	fixtures, err := testfixtures.NewFiles(repo.DB, &testfixtures.SQLite{},
		"fixtures/movie.yml",
		"fixtures/genre.yml",
		"fixtures/movie_has_genre.yml",
		"fixtures/person.yml",
		"fixtures/credits_cast.yml",
	)
	if err != nil {
		log.Fatalf("couldn't detetct fixtures directory: %v", err)
	}

	if err := fixtures.Load(); err != nil {
		log.Fatalf("couldn't load fixture files: %v", err)
	}

	return repo
}

func TestMovieRepository(t *testing.T) {
	t.Run("get movie", testGetMovie)
	t.Run("get movie error", testGetMovieError)
	t.Run("delete movie", testDeleteMovie)
	t.Run("create movie", testCreateMovie)
	t.Run("create movie error", testCreateMovieError)
	t.Run("create genres", testCreateGenre)
	t.Run("create movie genre connection", testCreateGenreMovieConnection)
	t.Run("create cast", testCreateCast)
	t.Run("create person", testCreatePerson)
	t.Run("get movies", testGetMovies)

	//cleanup
	os.Remove("privlib_test.db")
}

func testGetMovie(t *testing.T) {
	repo := prepareTestDatabase()

	movie, err := repo.GetMovie(1)

	assert.Nil(t, err)
	assert.Equal(t, &expectedMovie, movie)
}

func testGetMovieError(t *testing.T) {
	repo := prepareTestDatabase()

	movie, err := repo.GetMovie(4)
	assert.Equal(t, "couldn't get movie details with id 4: couldn't get movie entry from db: sql: no rows in result set", err.Error())
	assert.Equal(t, &models.MovieDetails{}, movie)
}

func testDeleteMovie(t *testing.T) {
	repo := prepareTestDatabase()

	err := repo.DeleteMovie(1)

	assert.Nil(t, err)
	assert.Equal(t, 2, countRows(repo, "movie"))
	assert.Equal(t, 1, countRows(repo, "movie_has_genre"))
	assert.Equal(t, 1, countRows(repo, "credits_cast"))
}

func testGetMovies(t *testing.T) {
	repo := prepareTestDatabase()

	params := models.MovieSearchParameter{Page: 0, Size: 0}
	movies, err := repo.GetMovies(params)

	assert.Nil(t, err)
	assert.Equal(t, 3, len(movies))
}

func testCreateMovie(t *testing.T) {
	repo := prepareTestDatabase()

	newMovie := expectedMovie
	newMovie.ID = 4
	newMovie.Title = "126"

	err := repo.CreateMovie(&newMovie)

	assert.Nil(t, err)
	assert.Equal(t, 4, countRows(repo, "movie"))
}

func testCreateMovieError(t *testing.T) {
	repo := prepareTestDatabase()

	err := repo.CreateMovie(&expectedMovie)

	assert.Equal(t, "couldn't execute movie insert statement: UNIQUE constraint failed: movie.id", err.Error())
	assert.Equal(t, 3, countRows(repo, "movie"))
}

func testCreateGenre(t *testing.T) {
	repo := prepareTestDatabase()

	var genres []*models.Genre = []*models.Genre{
		{ID: 3, Name: "Romantic"},
		{ID: 4, Name: "Comedy"},
	}

	err := repo.CreateGenre(genres)

	assert.Nil(t, err)
	assert.Equal(t, 4, countRows(repo, "genre"))
}

func testCreateGenreMovieConnection(t *testing.T) {
	repo := prepareTestDatabase()

	var genres []*models.Genre = []*models.Genre{
		{ID: 3, Name: "Romantic"},
		{ID: 4, Name: "Comedy"},
	}

	err := repo.CreateGenreMovieConnection(2, genres)

	assert.Nil(t, err)
	assert.Equal(t, 5, countRows(repo, "movie_has_genre"))
}

func testCreateCast(t *testing.T) {
	repo := prepareTestDatabase()

	var cast []models.CastDetails = []models.CastDetails{
		{ID: 3, Character: "Adam", Order: 0},
		{ID: 4, Character: "Kirk", Order: 1},
	}

	err := repo.CreateCast(2, cast)

	assert.Nil(t, err)
	assert.Equal(t, 3, countRows(repo, "movie_has_genre"))
}

func testCreatePerson(t *testing.T) {
	repo := prepareTestDatabase()

	var person models.Person = models.Person{
		ID:           3,
		Name:         "Adam",
		Birthday:     "2.3.1985",
		PlaceOfBirth: "St. Paul",
		Gender:       2,
		Biography:    "bio",
		Popularity:   5.36,
		Image:        "base64",
	}

	err := repo.CreatePerson(&person)

	assert.Nil(t, err)
	assert.Equal(t, 2, countRows(repo, "person"))
}

func countRows(repo *Repository, table string) (count int) {
	query := fmt.Sprintf("SELECT COUNT(*) AS count FROM %s", table)
	repo.DB.QueryRow(query).Scan(&count)
	return count
}
