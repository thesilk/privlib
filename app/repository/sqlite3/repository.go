package sqlite3

import (
	"database/sql"
	"fmt"
	"log"

	//"github.com/mattn/go-sqlite3" needs blank import
	_ "github.com/mattn/go-sqlite3"
	"github.com/pressly/goose"
)

type Repository struct {
	DB *sql.DB
}

const DATABASEDRIVER = "sqlite3"

func NewRepository(migrationPath, databaseFile string) *Repository {
	var (
		r   Repository
		err error
	)

	r.DB, err = sql.Open(DATABASEDRIVER, databaseFile)
	if err != nil {
		panic(fmt.Sprintf("Couldn't open %s: %v", databaseFile, err))
	}

	goose.SetDialect(DATABASEDRIVER)
	if err := goose.Up(r.DB, migrationPath); err != nil {
		panic(fmt.Sprintf("Couldn't migrate database: %v", err))
	}

	return &r
}

func NewTestRepository(migrationPath string) *Repository {
	var (
		r   Repository
		err error
	)

	r.DB, err = sql.Open(DATABASEDRIVER, "privlib_test.db")
	if err != nil {
		log.Fatalf(fmt.Sprintf("Couldn't open privlib_test.db database: %v", err))
	}

	goose.SetDialect(DATABASEDRIVER)
	if err := goose.Up(r.DB, migrationPath); err != nil {
		panic(fmt.Sprintf("Couldn't migrate database: %v", err))
	}

	return &r
}
