package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"

	"gitlab.com/thesilk/privlib/app/config"
	"gitlab.com/thesilk/privlib/app/controller"
	"gitlab.com/thesilk/privlib/app/controller/thirdparty"
	"gitlab.com/thesilk/privlib/app/repository/sqlite3"
	"gitlab.com/thesilk/privlib/app/usecase"
)

var (
	version       string
	configPath    string
	migrationPath string
	databasePath  string
)

func initCLI() {
	cwd, err := os.Getwd()
	if err != nil {
		log.Printf("couldn't get current working directory: %v", err)
	}

	helpFlag := flag.Bool("help", false, "help")
	versionFlag := flag.Bool("version", false, "version")
	flag.StringVar(&configPath, "config", fmt.Sprintf("%s/config.yaml", cwd), "path to privlib config yaml file")
	flag.StringVar(&migrationPath, "migrations", fmt.Sprintf("%s/migrations", cwd), "path to privlib database migration directory")
	flag.StringVar(&databasePath, "database", fmt.Sprintf("%s/privlib.db", cwd), "path to privlib database")
	flag.Parse()

	if *helpFlag {
		flag.Usage()
		os.Exit(0)
	}

	if *versionFlag {
		fmt.Println(version)
		os.Exit(0)
	}
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	initCLI()

	err := config.InitConfig(configPath)
	if err != nil {
		panic(err)
	}

	adapter := controller.NewAdapter()
	repository := sqlite3.NewRepository(migrationPath, databasePath)

	// httpClient := http.Client{Timeout: time.Second * 2}
	url := "https://api.themoviedb.org/3"
	apiKey := config.Cfg.GetAPIKey()
	movieApi := thirdparty.NewTheMovieDbAPI(url, apiKey)

	searchMovie := usecase.NewSearchMovie(movieApi)
	searchMovies := usecase.NewSearchMovies(movieApi)
	getMovie := usecase.NewGetMovie(repository)
	getMovies := usecase.NewGetMovies(repository)
	createMovie := usecase.NewCreateMovie(repository, movieApi)
	deleteMovie := usecase.NewDeleteMovie(repository)
	image := usecase.NewGetImage(thirdparty.Image{})

	searchMovieHandler := adapter.MakeSearchMovie(searchMovie)
	searchMoviesHandler := adapter.MakeSearchMovies(searchMovies)
	getMovieHandler := adapter.MakeGetMovie(getMovie)
	getMoviesHandler := adapter.MakeGetMovies(getMovies)
	createMovieHandler := adapter.MakeCreateMovie(createMovie, image)
	deleteMovieHandler := adapter.MakeDeleteMovie(deleteMovie)

	adapter.HandleFunc("/api/v0/search/movie/{id}", searchMovieHandler).Methods("GET")
	adapter.HandleFunc("/api/v0/search/movies", searchMoviesHandler).Methods("GET")
	adapter.HandleFunc("/api/v0/movies", getMoviesHandler).Methods("GET")
	adapter.HandleFunc("/api/v0/movie/{id}", getMovieHandler).Methods("GET")
	adapter.HandleFunc("/api/v0/movie/{id}", createMovieHandler).Methods("POST")
	adapter.HandleFunc("/api/v0/movie/{id}", deleteMovieHandler).Methods("DELETE")

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)
	go func() {
		adapter.ListenAndServe()
	}()
	<-stop

	fmt.Println()
	log.Println("Shutting down the server ...")
	adapter.Shutdown()
	log.Println("Server gracefully stopped")
}
