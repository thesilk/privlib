FROM debian:stretch-slim
MAINTAINER Christian Seidemann <thesilk@posteo.net>

#SHELL ["/bin/bash", "-c"]

RUN apt-get update && apt-get install -y ca-certificates curl make git gcc

# Install golang 1.14.3
RUN curl -O https://dl.google.com/go/go1.14.3.linux-amd64.tar.gz && \
    echo -n "1c39eac4ae95781b066c144c58e45d6859652247f7515f0d2cba7be7d57d2226 *go1.14.3.linux-amd64.tar.gz" | sha256sum -c && \
    tar xf go1.14.3.linux-amd64.tar.gz && \
    mv go /usr/local/ && \
    mkdir /go

ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/go/bin:/go/bin
ENV GOPATH=/go

RUN apt-get autoremove -y && apt-get clean
