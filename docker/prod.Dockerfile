FROM debian:bullseye-slim
MAINTAINER Christian Seidemann <thesilk@posteo.net>

ARG PRIVLIB_PACKAGE

RUN apt-get update && apt-get install -y ca-certificates
COPY $PRIVLIB_PACKAGE /tmp/privlib.deb

RUN dpkg -i /tmp/privlib.deb

CMD ["privlib"]
