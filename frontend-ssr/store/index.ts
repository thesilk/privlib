import Vue from 'vue';
import Vuex from 'vuex';
import SearchMovieModule from '@/store/search_movie';

Vue.use(Vuex);

export const store = new Vuex.Store({
  modules: {
    searchMovie: SearchMovieModule
  }
});
