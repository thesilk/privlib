import { Module, VuexModule, Action, Mutation } from 'vuex-module-decorators';
import axios from 'axios';
import { SearchMovie, PaginationProperty } from '~/interfaces/search_movie';

interface MovieProperty {
  movieName: string;
  paginationProperty: PaginationProperty;
}

@Module({ namespaced: true })
export default class SearchMovieModule extends VuexModule {
  searchMovies: SearchMovie[] = [];
  searchProperty: MovieProperty | null = null;

  get getSearchMovies(): SearchMovie[] {
    return this.searchMovies;
  }

  get getPaginationProperty(): PaginationProperty | null {
    if (this.searchProperty) {
      return this.searchProperty.paginationProperty;
    }
    return null;
  }

  get getSearchMovieName(): string | null {
    if (this.searchProperty) {
      return this.searchProperty.movieName;
    }
    return null;
  }

  @Mutation
  addMovies(movies: SearchMovie[]) {
    movies.forEach((item) => {
      this.searchMovies.push(item);
    });
  }

  @Mutation
  setSearchProperty(searchProperty: MovieProperty) {
    this.searchProperty = searchProperty;
  }

  @Mutation
  deleteMovies() {
    this.searchMovies.length = 0;
  }

  @Action
  async updateMovies(movieProperty: MovieProperty) {
    await axios
      .get(
        `http://localhost:2342/api/v0/search/movies?page=${movieProperty.paginationProperty.page}&size=${movieProperty.paginationProperty.pageSize}&movieName=${movieProperty.movieName}`
      )
      .then((res) => {
        this.context.commit('addMovies', res.data.Search);
        movieProperty.paginationProperty.total = +res.data.totalResults;
        this.context.commit('setSearchProperty', movieProperty);
      });
  }
}
