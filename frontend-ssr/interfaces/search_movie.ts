export interface SearchMovie {
  id: number;
  title: string;
  year?: string;
  poster?: string;
  image?: string;
  imdbID?: string;
  imdbRating?: string;
  type?: string;
}

export interface PaginationProperty {
  page: number;
  pageSize: number;
  total?: number;
}

export interface MovieDetails {
  title: string;
  year: string;
  rated: string;
  released: string;
  runtime: string;
  genre: string;
  director: string;
  writer: string;
  actors: string;
  plot: string;
  language: string;
  country: string;
  awards: string;
  metascore: string;
  imdbRating: string;
  imdbVotes: string;
  type: string;
  dvd: string;
  boxOffice: string;
  production: string;
  website: string;
  image: string;
}
