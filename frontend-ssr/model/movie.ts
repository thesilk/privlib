import { MovieDetails } from '@/interfaces/search_movie';

export class Movie {
  private movie: MovieDetails = {
    title: '',
    year: '',
    rated: '',
    released: '',
    runtime: '',
    genre: '',
    director: '',
    writer: '',
    actors: '',
    plot: '',
    language: '',
    country: '',
    awards: '',
    metascore: '',
    imdbRating: '',
    imdbVotes: '',
    type: '',
    dvd: '',
    boxOffice: '',
    production: '',
    website: '',
    image: '',
  };

  createMovieDetails(): MovieDetails {
    return this.movie;
  }
}
